# HTTP Benchmark

* [ab](#ab)
* [siege](#siege)
* [other tools](#other-tools)

## ab

A simple tool to perform a basic benchmark is __ab__ ([documentation](http://httpd.apache.org/docs/2.2/programs/ab.html)). Is part of Apache

> ab is a tool for benchmarking your Apache Hypertext Transfer Protocol (HTTP) server. It is designed to give you an impression of how your current Apache installation performs. This especially shows you how many requests per second your Apache installation is capable of serving.

#### Installation

__Apache installed__

If you have access to a Linux server, chances are you may already have a really simple http load generating tool installed called Apache Bench, or ab. 

__Apache not installed__

```
apt-get install apache2-utils
```

#### Usage

```
ab -n <num_requests> -c <concurrency> <addr>:<port><path>
```

__Example__

```
ab -n 100 -c 10 http://example.com/
```

__Output__

```
This is ApacheBench, Version 2.3 <$Revision: 1638069 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking example.com (be patient).....done


Server Software:        Apache/2.2
Server Hostname:        example.com
Server Port:            80

Document Path:          /
Document Length:        262 bytes

Concurrency Level:      10
Time taken for tests:   1.107 seconds
Complete requests:      100
Failed requests:        10
Total transferred:      106960 bytes
HTML transferred:       26180 bytes
Requests per second:    90.34 [#/sec] (mean)
Time per request:       110.688 [ms] (mean)
Time per request:       11.069 [ms] (mean, across all concurrent requests)
Transfer rate:          94.37 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        9   11   1.4     10      19
Processing:    34   93  15.6     94     117
Waiting:       34   68  20.3     68     117
Total:         45  103  15.9    104     128

Percentage of the requests served within a certain time (ms)
  50%    104
  66%    106
  75%    109
  80%    110
  90%    118
  95%    127
  98%    127
  99%    128
 100%    128 (longest request)
```

__Output to a file__

ab has an option to export the results to a file in gnuplot and csv format. This is useful to graphic the results with an online gnuplot tool like [http://gnuplot.respawned.com/](http://gnuplot.respawned.com/). An example with fake data could look like this:

![out.svg](out.svg)

__Real example__: 5 concurrent users each doing 10 page hits

```
ab -r -n 50 -c 10 -k -e toolbar.csv http://localhost:5000/toolbar/email@server.com/
```

#### Posts about ab

* [https://www.devside.net/wamp-server/load-testing-apache-with-ab-apache-bench](https://www.devside.net/wamp-server/load-testing-apache-with-ab-apache-bench)
* [http://infoheap.com/ab-apache-bench-load-testing/](http://infoheap.com/ab-apache-bench-load-testing/)
* [https://www.petefreitag.com/item/689.cfm](https://www.petefreitag.com/item/689.cfm)
* [https://www.digitalocean.com/community/tutorials/how-to-use-apachebench-to-do-load-testing-on-an-ubuntu-13-10-vps](https://www.digitalocean.com/community/tutorials/how-to-use-apachebench-to-do-load-testing-on-an-ubuntu-13-10-vps)

## siege

Another tool to perform http benchmark is __siege__ ([documentation](https://www.joedog.org/siege-home/))

> Siege is an http load testing and benchmarking utility. It was designed to let web developers measure their code under duress, to see how it will stand up to load on the internet

__Info__

* [Github repo](https://github.com/JoeDog/siege)
* Latest release: 2015, Jul 9 (version 3.1.1)

#### Installation

If you are using Ubuntu or Debian

```
apt-get install siege
```

If you are using CentOS/Fedora use this method

```
yum install siege
```

Otherwise, you can [install](https://github.com/JoeDog/siege/blob/master/INSTALL) from [source](https://github.com/JoeDog/siege/releases)

```
wget url/to/latest/release
tar -xvpzf package-version.tar.gz
cd package-version/
./configure
make
make install
```

#### Usage

```
siege -d<delay> -c<concurrency> -t<time>s <addr>:<port><path>
```

__Example__

```
siege -d10 -c50 -t60s http://example.com/
```

__Output__

```
** SIEGE 3.1.1
** Preparing 15 concurrent users for battle.
The server is now under siege...
Lifting the server siege...      done.

Transactions:                442 hits
Availability:             100.00 %
Elapsed time:              59.43 secs
Data transferred:           0.54 MB
Response time:              0.37 secs
Transaction rate:           7.44 trans/sec
Throughput:             0.01 MB/sec
Concurrency:                2.72
Successful transactions:         442
Failed transactions:               0
Longest transaction:            0.40
Shortest transaction:           0.36
```

#### Posts about siege

* [http://www.nginxtips.com/nginx-benchmarking-using-siedge/](http://www.nginxtips.com/nginx-benchmarking-using-siedge/)
* [http://blog.remarkablelabs.com/2012/11/benchmarking-and-load-testing-with-siege/](http://blog.remarkablelabs.com/2012/11/benchmarking-and-load-testing-with-siege/)

## Posts comparing ab and siege

* [http://tweaked.io/benchmarking/](http://tweaked.io/benchmarking/)
* [https://kalamuna.atlassian.net/wiki/display/KALA/Testing+With+Aphe+Benchmark+and+Siege](https://kalamuna.atlassian.net/wiki/display/KALA/Testing+With+Apache+Benchmark+and+Siege)

## Other tools

* [httperf](http://www.hpl.hp.com/research/linux/httperf/)
* [weighttp](http://redmine.lighttpd.net/projects/weighttp/wiki)
* [httpress](https://bitbucket.org/yarosla/httpress/wiki/Home)
* [JMeter](http://jmeter.apache.org/)
* [tourbus](https://github.com/dbrady/tourbus)
* [blitz](https://www.blitz.io/) (Cloud based)
* [loader](http://loader.io/) (Cloud based)
* [loadimpact](https://loadimpact.com/) (Cloud based)

